//
//  StringExtension.swift
//  PrimitiveExtension
//
//  Created by Michael Dyachenko on 30/06/15.
//  Copyright (c) 2015 AppBit. All rights reserved.
//

import UIKit

private let lowerIndexList = ["₀", "₁", "₂", "₃", "₄", "₅", "₆", "₇", "₈", "₉"]
private let upperIndexList = ["⁰", "¹", "²", "³", "⁴", "⁵", "⁶", "⁷", "⁸", "⁹"]

extension String {
    
    init(repeatValue : String, amount : Int) {
        var result = ""
        for _ in 0 ..< amount {
            result += repeatValue
        }
        
        self = result
    }
    
    // MARK: - String partition methods
    
    var length : Int {
        get {
            return characters.count
        }
    }
    
    func characterAtIndex(index : Int) -> Character {
        return Array(characters)[index]
    }
    
    subscript (index: Int) -> Character {
        return characterAtIndex(index)
    }
    
    subscript (range: Range<Int>) -> String {
        let startIndex = self.startIndex.advancedBy(range.startIndex)
        let endIndex = self.startIndex.advancedBy(range.endIndex, limit: self.endIndex)
        
        if startIndex >= endIndex {
            return ""
        }
        
        return substringWithRange(startIndex ..< endIndex)
    }
    
    func substringToIndex(index: Int) -> String {
        guard index > 0 else {
            return ""
        }
        
        guard index < length else {
            return self
        }
        
        return substringToIndex(startIndex + index)
    }
    
    func substringFromIndex(index: Int) -> String {
        guard index > 0 else {
            return self
        }
        
        guard index < length else {
            return ""
        }
        
        return substringFromIndex(startIndex + index)
    }
    
    // MARK: - Modification methods
    
    func addAmountValue(amount : Int, singleSuffix : String = "", multiSuffix : String = "s") -> String {
        if amount == 1 {
            return "\(amount) \(self)\(singleSuffix)"
        }
        else {
            return "\(amount) \(self)\(multiSuffix)"
        }
    }
    
    func stringByReplacingCharactersInRange(range : NSRange, withString replaceString : String) -> String {
        let startIndex = self.startIndex + range.location
        let endIndex = self.startIndex + range.location + range.length
        
        return stringByReplacingCharactersInRange(startIndex ..< endIndex, withString: replaceString)
    }
    
    func toFloat(separateSymbol : Character = ".") -> CGFloat {
        let components = componentsSeparatedByString(String(separateSymbol))
        
        let integerPart = Int(components[0]) ?? 0
        let fractionalPart : Int
        if components.count < 2 {
            fractionalPart = 0
        }
        else {
            fractionalPart = Int(components[1]) ?? 0
        }
        
        var realFractionalPart = CGFloat(fractionalPart)
        while realFractionalPart >= 1 {
            realFractionalPart /= 10
        }
        
        return CGFloat(integerPart) + realFractionalPart
    }
    
    // MARK: - Integer index methods
    
    init(lowerIndex : Int) {
        self = String(parseIntValue: lowerIndex, componentList: lowerIndexList)
    }
    
    init(upperIndex : Int) {
        self = String(parseIntValue: upperIndex, componentList: upperIndexList)
    }
    
    private init(parseIntValue : Int, componentList : [String]) {
        let digitList = parseIntValue.digitList
        
        if parseIntValue < 0 {
            self = "-" + digitList.map({ componentList[$0] }).joinWithSeparator("")
        }
        else {
            self = digitList.map({ componentList[$0] }).joinWithSeparator("")
        }
    }
    
}

func +(lhs : String.Index, rhs : Int) -> String.Index {
    return lhs.advancedBy(rhs)
}

func -(lhs : String.Index, rhs : Int) -> String.Index {
    return lhs.advancedBy(-rhs)
}

extension Character {
    
    var isDigit : Bool {
        return (self >= "0") && (self <= "9")
    }
    
    var isUppercaseLetter : Bool {
        return (self >= "A") && (self <= "Z")
    }
    
    var isLowercaseLetter : Bool {
        return (self >= "a") && (self <= "z")
    }
    
}
