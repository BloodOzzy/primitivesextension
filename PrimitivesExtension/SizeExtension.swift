//
//  SizeExtension.swift
//  PrimitiveExtension
//
//  Created by Michael Dyachenko on 16/11/15.
//  Copyright (c) 2015 AppBit. All rights reserved.
//

import UIKit

func -(lhs : CGSize, rhs : CGSize) -> CGSize {
    return CGSize(width: lhs.width - rhs.width, height: lhs.height - rhs.height)
}

func -(lhs : CGSize, rhs : CGFloat) -> CGSize {
    return CGSize(width: lhs.width - rhs, height: lhs.height - rhs)
}

func -(lhs : CGFloat, rhs : CGSize) -> CGSize {
    return CGSize(width: lhs - rhs.width, height: lhs - rhs.height)
}

func +(lhs : CGSize, rhs : CGSize) -> CGSize {
    return CGSize(width: lhs.width + rhs.width, height: lhs.height + rhs.height)
}

func +(lhs : CGSize, rhs : CGFloat) -> CGSize {
    return CGSize(width: lhs.width + rhs, height: lhs.height + rhs)
}

func +(lhs : CGFloat, rhs : CGSize) -> CGSize {
    return CGSize(width: lhs + rhs.width, height: lhs + rhs.height)
}

func *(lhs : CGSize, rhs : CGSize) -> CGSize {
    return CGSize(width: lhs.width * rhs.width, height: lhs.height * rhs.height)
}

func *(lhs : CGFloat, rhs : CGSize) -> CGSize {
    return CGSize(width: lhs * rhs.width, height: lhs * rhs.height)
}

func *(lhs : CGSize, rhs : CGFloat) -> CGSize {
    return rhs * lhs
}

func /(lhs : CGSize, rhs : CGSize) -> CGSize {
    return CGSize(width: lhs.width / rhs.width, height: lhs.height / rhs.height)
}

extension CGSize {
    init(value : CGFloat) {
        self = CGSize(width: value, height: value)
    }
    
    var point : CGPoint {
        return CGPoint(x: width, y: height)
    }
}