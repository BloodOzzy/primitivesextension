//
//  FloatExtension.swift
//  PrimitiveExtension
//
//  Created by Michael Dyachenko on 30/06/15.
//  Copyright (c) 2015 AppBit. All rights reserved.
//

import UIKit

extension CGFloat {
    
    func stringValue(fractionSize fractionSize : Int, showExcessZeros : Bool = false) -> String {
        guard fractionSize > 0 else {
            return "\(Int(round(self)))"
        }
        
        let multiplayer = pow(10, CGFloat(fractionSize))
        let showValue = Int(round(self * multiplayer))
        
        let integerResultPart = "\(showValue / Int(multiplayer))"
        
        var showFraction = showValue % Int(multiplayer)
        while !showExcessZeros && showFraction > 0 && showFraction % 10 == 0 {
            showFraction /= 10
        }
        
        let fractionResultPart : String
        if showFraction == 0 {
            if showExcessZeros {
                fractionResultPart = "." + String(repeatValue: "0", amount: fractionSize)
            }
            else {
                fractionResultPart = ""
            }
        }
        else {
            fractionResultPart = ".\(showFraction)"
        }
        
        return integerResultPart + fractionResultPart
    }
    
}
