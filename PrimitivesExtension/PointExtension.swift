//
//  PointExtension.swift
//  PrimitiveExtension
//
//  Created by Michael Dyachenko on 06/01/15.
//  Copyright (c) 2015 AppBit. All rights reserved.
//

import UIKit

extension CGPoint {
    
    init(angle : CGFloat) {
        self.x = cos(angle)
        self.y = sin(angle)
    }
    
    init(angle : Double) {
        self.x = CGFloat(cos(angle))
        self.y = CGFloat(sin(angle))
    }
    
    init(rotatePoint : CGPoint, angle : CGFloat) {
        self.x = rotatePoint.x * cos(angle) - rotatePoint.y * sin(angle)
        self.y = rotatePoint.x * sin(angle) + rotatePoint.y * cos(angle)
    }
    
    var vectorLength : CGFloat {
        get {
            return sqrt(pow(x, 2) + pow(y, 2))
        }
        set {
            let multRatio = newValue / vectorLength
            self = multRatio * self
        }
    }
    
    var vector : CGVector {
        get {
            return CGVector(dx: x, dy: y)
        }
    }
    
    var size : CGSize {
        get {
            return CGSize(width: x, height: y)
        }
    }
    
    var reverse : CGPoint {
        get {
            return CGPoint(x: y, y: x)
        }
    }
    
    var round : CGPoint {
        get {
            return CGPoint(x: floor(x + 0.5), y: floor(y + 0.5))
        }
    }
}

func ==(lhs: CGPoint, rhs: CGPoint) -> Bool {
    return (lhs.x == rhs.x && lhs.y == rhs.y)
}

prefix func -(lhs : CGPoint) -> CGPoint {
    return -1 * lhs
}

func +(lhs: CGPoint, rhs: CGPoint) -> CGPoint {
    return CGPoint(x: lhs.x + rhs.x, y: lhs.y + rhs.y)
}

func +(lhs: CGPoint, rhs: CGFloat) -> CGPoint {
    return CGPoint(x: lhs.x + rhs, y: lhs.y + rhs)
}

func +(lhs: CGFloat, rhs: CGPoint) -> CGPoint {
    return rhs + lhs
}

func -(lhs: CGPoint, rhs: CGFloat) -> CGPoint {
    return CGPoint(x: lhs.x - rhs, y: lhs.y - rhs)
}

func -(lhs : CGFloat, rhs : CGPoint) -> CGPoint {
    return CGPoint(x: lhs - rhs.x, y: lhs - rhs.y)
}

func -(lhs: CGPoint, rhs: CGPoint) -> CGPoint {
    return CGPoint(x: lhs.x - rhs.x, y: lhs.y - rhs.y)
}

func *(lhs : CGPoint, rhs : CGPoint) -> CGPoint {
    return CGPoint(x: lhs.x * rhs.x, y: lhs.y * rhs.y)
}

func *(lhs: CGFloat, rhs: CGPoint) -> CGPoint {
    return CGPoint(x: lhs * rhs.x, y: lhs * rhs.y)
}

func *(lhs: CGPoint, rhs: CGFloat) -> CGPoint {
    return CGPoint(x: lhs.x * rhs, y: lhs.y * rhs)
}

func /(lhs: CGPoint, rhs: CGPoint) -> CGPoint {
    return CGPoint(x: lhs.x / rhs.x, y: lhs.y / rhs.y)
}

func /(lhs: CGPoint, rhs: CGFloat) -> CGPoint {
    return CGPoint(x: lhs.x / rhs, y: lhs.y / rhs)
}