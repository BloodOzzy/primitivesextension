//
//  RectExtension.swift
//  PrimitiveExtension
//
//  Created by Michael Dyachenko on 06/01/15.
//  Copyright (c) 2015 AppBit. All rights reserved.
//

import UIKit

extension CGRect {
    init(center : CGPoint, size : CGSize) {
        self.origin = CGPoint(x: center.x - 0.5 * size.width, y: center.y - 0.5 * size.height)
        self.size = size
    }
    
    var diagonal : CGFloat {
        return sqrt(pow(width, 2) + pow(height, 2))
    }
    
    var center : CGPoint {
        return CGPoint(x: midX, y: midY)
    }
    
    var roundRect : CGRect {
        let minX : CGFloat = round(origin.x)
        let minY : CGFloat = round(origin.y)
        let maxX : CGFloat = round(self.maxX)
        let maxY : CGFloat = round(self.maxY)
        
        return CGRect(x: minX, y: minY, width: maxX - minX, height: maxY - minY)
    }
}

func *(lhs : CGFloat, rhs : CGRect) -> CGRect {
    return CGRect(x: lhs * rhs.origin.x, y: lhs * rhs.origin.y, width: lhs * rhs.width, height: lhs * rhs.height)
}

func *(lhs : CGRect, rhs : CGFloat) -> CGRect {
    return rhs * lhs
}

func +(lhs : CGRect, rhs : CGRect) -> CGRect {
    return CGRect(x: lhs.origin.x + rhs.origin.x, y: lhs.origin.y + rhs.origin.y, width: lhs.width + rhs.width, height: lhs.height + rhs.height)
}