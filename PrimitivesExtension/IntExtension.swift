//
//  IntExtension.swift
//  PrimitiveExtension
//
//  Created by Michael Dyachenko on 30/06/15.
//  Copyright (c) 2015 AppBit. All rights reserved.
//

import UIKit

extension Int {
    
    var digitAmount : Int {
        return Int(floor(log10(CGFloat(self)))) + 1
    }
    
    var digitList : [Int] {
        guard self != 0 else {
            return [0]
        }
        
        var digitList : [Int] = []
        var parseValue = abs(self)
        while parseValue != 0 {
            let digit = parseValue % 10
            parseValue /= 10
            
            digitList.append(digit)
        }
        
        return digitList.reverse()
    }
    
}
