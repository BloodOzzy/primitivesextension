Pod::Spec.new do |s|

  s.name         = "PrimitivesExtension"
  s.version      = "0.1.0"
  s.summary      = "Extension for primitive types"

#s.description  = <<-DESC
#                 DESC

  s.homepage     = "https://bitbucket.org/BloodOzzy/primitivesextension"
  # s.screenshots  = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"
  s.license      = { :type => "MIT", :file => "LICENSE" }

  s.author             = { "BloodOzzy" => "archmag.kreol@yandex.ru" }
  # s.social_media_url   = "http://twitter.com/AltX"

  s.platform     = :ios, "9.0"
  s.source       = { :git => "https://bitbucket.org/BloodOzzy/primitivesextension.git", :tag => "#{s.version}" }


  s.source_files  = "PrimitivesExtension/*.{h,m,swift}"
  # s.exclude_files = "Classes/Exclude"
  # s.public_header_files = "Classes/**/*.h"

  # s.resource  = "icon.png"
  # s.resources = "Resources/*.png"

  # s.preserve_paths = "FilesToSave", "MoreFilesToSave"

  s.framework  = "UIKit"
  # s.frameworks = "SomeFramework", "AnotherFramework"

  # s.library   = "iconv"
  # s.libraries = "iconv", "xml2"


  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

  # s.requires_arc = true

  # s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  # s.dependency "JSONKit", "~> 1.4"

end
